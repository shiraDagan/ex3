<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
            'title' => 'War and Peace',
            'author' => 'Leo Tolstoy',
            'created_at' => date('Y-m-d G:i:s'),
            ],
        
            [
                'title' => 'Song of Solomon',
                'author' => 'Toni Morrison',
                'created_at' => date('Y-m-d G:i:s'),
            ],
        
            [
                    'title' => 'Ulysses',
                    'author' => 'James Joyce',
                    'created_at' => date('Y-m-d G:i:s'),
            ],

            [
                'title' => 'The Shadow of the Wind',
                'author' => 'Carlos Ruiz Zafon',
                'created_at' => date('Y-m-d G:i:s'),
        ],

        [
            'title' => 'The Lord of the Rings',
            'author' => 'J.R.R. Tolkien',
            'created_at' => date('Y-m-d G:i:s'),
    ]
            ]);


        }
}
